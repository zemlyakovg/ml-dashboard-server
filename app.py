from flask import Flask, jsonify
from flask_cors import CORS, cross_origin

from data.CSVDataProvider import CSVDataProvider

app = Flask(__name__)
cors = CORS(app)
fifads = CSVDataProvider('./data/fifa.csv')

@app.route("/options", methods=["GET"])
@cross_origin()
def options():
    return jsonify({
        "opts":fifads.options(),
        "nopts":fifads.numeric_options()
    })

@app.route("/linear/<x>/<y>/<color_code>", methods=["GET"])
@cross_origin()
def linear_chart(x, y, color_code):
    return jsonify(fifads.linear_chart(x, y))

@app.route("/kmeans/<n_clusters>/<xs>", methods=["GET"])
@cross_origin()
def kmeans(n_clusters, xs):
    return jsonify(fifads.kmeans(int(n_clusters), xs.split('|')))

@app.route("/tsne/<xs>", methods=["GET"])
@cross_origin()
def tsne(xs):
    return jsonify(fifads.tsne(xs.split('|')))

if __name__ == '__main__':
    app.run(debug=True)