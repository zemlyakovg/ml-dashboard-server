import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE

from data.DataProvider import DataProvider

class CSVDataProvider(DataProvider):

    def __init__(self, filepath):
        df = pd.read_csv(filepath, low_memory=False)
        self.df = df.drop(df.columns[0], axis=1).dropna(how='any',axis=0) 
        
    def options(self):
        return list(self.df)

    def numeric_options(self):
        return list(self.df._get_numeric_data())

    def linear_chart(self, x, y):
        sample = self.df.sample(100)
        return {
            'x': list(sample[x]),
            'y': list(sample[y])
        }

    def kmeans(self, n_clusters, xs):
        sample = self.df.sample(600)
        pca = PCA(n_components=2)
        reduced_data = pca.fit_transform(sample[xs].apply(pd.to_numeric, errors='coerce', axis=1))
        kmeans = KMeans(n_clusters=n_clusters).fit(reduced_data)
        datasets = [[] for i in range(n_clusters)]
        for item, f, cluster in zip(sample.iterrows(), reduced_data.tolist(), kmeans.labels_.tolist()):
            item = item[1]
            datasets[cluster].append({'x': f[0], 'y': f[1], 'tooltip': '%s %s %s' % (item['Name'], item['Preferred Positions'], item['Club'])})
        return {
            'data': datasets,
            'centroids': list(map(lambda c: {'x': c[0], 'y': c[1]}, kmeans.cluster_centers_.tolist())),
            'pca': reduced_data.tolist()
        }

    def tsne(self, xs):
        sample = self.df.sample(600)
        tsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=300)
        tsne_results = tsne.fit_transform(sample[xs].apply(pd.to_numeric, errors='coerce', axis=1))
        dataset = []
        for item, f in zip(sample.iterrows(), tsne_results.tolist()):
            item = item[1]
            dataset.append({'x': f[0], 'y': f[1], 'tooltip': '%s %s %s' % (item['Name'], item['Preferred Positions'], item['Club'])})
        return {
            'data': dataset
        }         