from abc import ABC, abstractmethod

class DataProvider(ABC):

    @abstractmethod
    def options(self):
        pass

    @abstractmethod
    def numeric_options(self):
        pass

    @abstractmethod
    def linear_chart(self, x, y):
        pass

    @abstractmethod
    def kmeans(self, n_clusters, xs):
        pass

    @abstractmethod
    def tsne(self, xs):
        pass